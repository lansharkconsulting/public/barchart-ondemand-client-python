Python client for Barchart OnDemand Changelog
---------------------------------------------
- 0.0.7 - drop Cython, python 3.4 and python 3.5 support

- 0.0.6 - make getFinancialHighlights optional
  * update dependencies
  * make getFinancialHighlights optional

- 0.0.5 - dependency updates

- 0.0.4 - API changes
  * Change API to pass apikey and url
  * Correct setup.py to use README.rst as long_description
  * update requirements

- 0.0.3 - Initial public release on pypi <ssharkey@lanshark.com>
  * removed panda support
  * added limited getFinancialHighlights
  * added optional (fields) on getQuote/getHistory
  * prepared for pypi

- 0.0.2 - femtotrader's original version
  * has panda support, no getFinancialHighlights, no options on getQuote/getHistory

